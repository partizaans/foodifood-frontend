FROM node:13.12.0-alpine
WORKDIR /app
COPY package.json /app/package.json
RUN yarn install
COPY src/ /app/src/
COPY public/ /app/public/
RUN yarn build
RUN yarn global add serve
RUN serve --help
CMD ["serve", "-s", "-l", "3000", "build"]
