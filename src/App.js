import React, {lazy, Suspense} from 'react';
import {BrowserRouter as Router, Route, Switch} from 'react-router-dom';

import './index';
import Restaurant from "./Restaurant";
import Signup from "./Signup";
import Home from "./Home";
import Login from "./Login";
import axios from "axios";

const Profile = lazy(() => import('./Profile/'));


export class App extends React.Component {
  render() {
    return (
      <Router>
        <Suspense fallback={<div>Loading...</div>}>
          <Switch>
            <Route exact path="/" component={Home}/>
            <Route exact path="/profile" component={Profile}/>
            <Route exact path="/restaurants/:id" render={props => (<Restaurant {...props} />)}/>
            <Route exact path="/signup" render={props => (<Signup {...props} />)}/>
            <Route exact path="/login" render={props => (<Login {...props} />)}/>
          </Switch>
        </Suspense>
      </Router>
    );
  }

  componentDidMount() {
    axios.defaults.headers.common['Authorization'] = localStorage.getItem("token")
  }
}
