import React from "react";
import ReactDOM from "react-dom";
import axios from 'axios'
import {withRouter} from "react-router";


import {Footer, Navbar} from "../common/Common";

class Login extends React.Component {
  constructor(props) {
    super(props);
    this.handleEmail = this.handleEmail.bind(this)
    this.handlePassword = this.handlePassword.bind(this)
    this.onSubmit = this.onSubmit.bind(this)
    this.state = {email: "", password: ""}
  }

  render() {
    return <div className="container h-100">
      <div className="jumbotron vh-100 text-center bg-white d-flex flex-column justify-content-center">
        <form onSubmit={this.onSubmit}>
          <div className="form align-items-center">
            <div className="form-group">
              <label htmlFor="email">ایمیل</label>
              <input type="email" className="form-control" id="email" placeholder="ایمیل خود را وارد کنید!"
                     onChange={this.handleEmail} name="email" required/>
            </div>
            <div className="form-group">
              <label htmlFor="password">رمز عبور</label>
              <input type="password" className="form-control" id="password" placeholder="رمز عبور خود را وارد کنید!"
                     onChange={this.handlePassword} name="password" required minLength="4"/>
            </div>
            <button type="submit" className="btn text-light bg-cyan">ورود</button>
          </div>
        </form>
      </div>
    </div>
  }

  onSubmit(event) {
    event.preventDefault()
    axios.post("http://localhost:8080/login", this.state)
      .then((response) => {
        localStorage.setItem("token", response.data.token)
        axios.defaults.headers.common['Authorization'] = response.data.token;
        alert('Success!')
        window.location.href = '/profile'
      }).catch(function (error) {
      alert(error.response.data.message)
    })
  }

  handleEmail(event) {
    let val = event.target.value
    this.setState(() => ({email: val}));
  }

  handlePassword(event) {
    let val = event.target.value
    this.setState(() => ({password: val}));
  }

}

ReactDOM.render(<Footer/>, document.getElementById('footer'))
ReactDOM.render(<Navbar/>, document.getElementById('navbar'))
export default withRouter(Login)
