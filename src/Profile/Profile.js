import React from 'react';
import axios from 'axios'
import './index';
import PropTypes from "prop-types";
import {PersianNumber} from 'react-persian-currency';
import {AuthenticatedComponent} from "../common/Common";


class TabOptions {
  static INCREASE_TAB = false
  static  ORDERS_TAB = !this.INCREASE_TAB
}

export class Profile extends AuthenticatedComponent {
  constructor(props) {
    super(props);
    this.state = {credit: 0, currentTab: TabOptions.ORDERS_TAB}
    this.onCreditChange = this.onCreditChange.bind(this)
    this.onTabChange = this.onTabChange.bind(this)
  }

  render() {
    return (
      <div>
        <div className="bg-pink text-light p-5">
          <ProfileInfo credit={this.state.credit}/>
        </div>
        <div className="p-5"/>
        <div className="my-5 card w-75 rounded-5 mx-auto">
          <div className="w-75 mx-auto top-50">
            <ProfileTabSelector setStateCallback={this.onTabChange}/>
          </div>
          <div className="card-body p-5 pt-0 mx-5 h3 font-weight-light">
            <IncreaseCreditTab changeCreditCallback={this.onCreditChange}
                               show={this.state.currentTab === TabOptions.INCREASE_TAB}/>
            <OrderListTab show={this.state.currentTab === TabOptions.ORDERS_TAB}/>
          </div>
        </div>
      </div>
    );
  }

  onCreditChange(credit) {
    this.setState(() => ({credit: credit}))
  }

  onTabChange(state) {
    this.setState(() => ({currentTab: state}))
  }
}


class ProfileInfo extends React.Component {
  constructor(props) {
    super(props);
    this.state = {fullName: "", phone: "", email: "", credit: props.credit}
  }

  componentWillReceiveProps(nextProps, nextContext) {
    if (nextProps.credit > 0)
      this.setState(() => ({credit: nextProps.credit}))
  }

  render() {
    return (
      <div className="row">
        <div className="col my-auto">
          <div className="h1">
            <i className="flaticon-account fa-2x"/>
            <span className="align-text-top">{this.state.fullName}</span>
          </div>
        </div>
        <div className="col-auto h2 font-weight-light pr-5 my-auto">
          <table className="table table-borderless">
            <tr>
              <td><i className="flaticon-phone"/></td>
              <td><PersianNumber>{this.state.phone}</PersianNumber></td>
            </tr>
            <tr>
              <td><i className="flaticon-mail"/></td>
              <td>{this.state.email}</td>
            </tr>
            <tr>
              <td><i className="flaticon-card"/></td>
              <td>
                <PersianNumber>{this.state.credit}</PersianNumber> تومان
              </td>
            </tr>
          </table>
        </div>
      </div>
    )
  }

  componentDidMount() {
    axios.get('http://localhost:8080/user')
      .then(response => this.setState({
        fullName: response.data.fullName,
        phone: response.data.phone,
        email: response.data.email,
        credit: response.data.credit
      }));
  }
}

class ProfileTabSelectorButton extends React.Component {
  constructor(props) {
    super(props);
    this.state = {show: false}
  }

  render() {
    if (this.props.active) {
      return (
        <button className="btn border btn-lg bg-pink p-3 py-4 text-light w-50">
          <span className="h3">{this.props.title}</span>
        </button>
      )
    }
    return (
      <button className="btn border btn-lg bg-white p-3 py-4 w-50" onClick={() => this.props.callback()}>
        <span className="h2 font-weight-normal">{this.props.title}</span>
      </button>
    )
  }
}

ProfileTabSelectorButton.propTypes = {
  active: PropTypes.bool,
  title: PropTypes.string,
  state: PropTypes.bool,
  callback: PropTypes.func,
};


class ProfileTabSelector extends React.Component {
  constructor(props) {
    super(props);
    this.state = {currentTab: TabOptions.ORDERS_TAB}
    this.switch = this.switch.bind(this);
  }

  render() {
    return (
      <div className="btn-group btn-group-lg btn-block shadow-sm">
        <ProfileTabSelectorButton callback={this.switch} title="سفارش‌ها"
                                  active={this.state.currentTab === TabOptions.ORDERS_TAB}
                                  stateID={TabOptions.ORDERS_TAB}/>
        <ProfileTabSelectorButton callback={this.switch} title="افزایش اعتبار"
                                  active={this.state.currentTab === TabOptions.INCREASE_TAB}
                                  stateID={TabOptions.INCREASE_TAB}/>
      </div>
    )
  }

  switch() {
    this.setState(() => ({currentTab: !this.state.currentTab}))
    this.props.setStateCallback(!this.state.currentTab)
  }
}


class IncreaseCreditTab extends React.Component {
  constructor(props) {
    super(props);
    this.handleAmount = this.handleAmount.bind(this)
    this.performIncrease = this.performIncrease.bind(this)
    this.state = {amount: null, show: props.show}
  }

  render() {
    return (
      <form className={"form-group w-auto" + (this.state.show ? '' : ' d-none')} onSubmit={this.performIncrease}>
        <div className="row w-75 mx-auto">
          <div className="col-xl-8 my-xl-5 my-3">
            <input className="form-control form-control-lg p-3 h-auto bg-light display-1 border-thick" min="0"
                   type="number" placeholder="میزان افزایش اعتبار" onChange={this.handleAmount} onSubmit="reset"/>
          </div>
          <div className="col-xl-4 my-xl-5 my-3">
            <button className="btn btn-lg btn-cyan btn-block p-3 px-xl-5">
              <span className="h3">افزایش</span>
            </button>
          </div>
        </div>
      </form>
    )
  }

  handleAmount(event) {
    let val = event.target.value
    this.setState(() => ({amount: val}));
  }

  performIncrease(event) {
    event.preventDefault()
    event.target.reset()
    axios.post("http://localhost:8080/increase-credit", {"credit": parseInt(this.state.amount)})
      .then((response) => {
        this.props.changeCreditCallback(response.data.credit)
      })
  }

  componentWillReceiveProps(nextProps, nextContext) {
    this.setState(() => ({show: nextProps.show}))
  }
}

IncreaseCreditTab.propTypes = {
  changeCreditCallback: PropTypes.func
}

class OrderListTab extends React.Component {
  constructor(props) {
    super(props);
    this.state = {show: props.show, orders: []}
  }

  render() {
    let toBeRendered = []
    for (const [index, order] of this.state.orders.entries()) {
      if (order.restaurantName == null) {
        continue
      }
      toBeRendered.push(<OrderItem number={index + 1} restaurantName={order.restaurantName} state={order.state}/>)
    }
    return (
      <div className={(this.state.show ? '' : ' d-none')}>
        {toBeRendered}
      </div>
    )
  }

  componentDidMount() {
    axios.get('http://localhost:8080/carts')
      .then(response => this.setState({
        orders: response.data.items
      }))
  }

  componentWillReceiveProps(nextProps, nextContext) {
    this.setState(() => ({show: nextProps.show}))
  }
}

class OrderItem extends React.Component {
  render() {
    return (
      <div className="row border bg-light rounded-5 p-0 text-center my-5">
        <div className="col-1 border-right p-3"><PersianNumber>{this.props.number}</PersianNumber></div>
        <div className="col-6 border-right p-3">{this.props.restaurantName}</div>
        <div className="col-5 p-3">
          <OrderItemState orderState={this.props.state}/>
        </div>
      </div>
    )
  }
}

OrderItem.propTypes = {
  number: PropTypes.number,
  restaurantName: PropTypes.string,
  state: PropTypes.string,
}

class OrderItemState extends React.Component {
  render() {
    switch (this.props.orderState) {
      case "DELIVERED":
        return <span className="btn btn-yellow rounded px-4 shadow-sm">مشاهده فاکتور</span>
      case "ASSSIGNED":
        return <span className="btn btn-green rounded px-4">پیک در مسیر</span>
      case "FINALIZED":
        return <span className="btn btn-light-cyan rounded px-4">در جستجوی پیک</span>
      case "UNFINALIZED":
        return <span className="btn btn-light-cyan rounded px-4">در انتظار پرداخت</span>
      default:
        return <></>

    }
  }
}

