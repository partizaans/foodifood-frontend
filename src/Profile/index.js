// import React from 'react';
import * as serviceWorker from '../common/serviceWorker';
import ReactDOM from "react-dom";
import React from "react";
import {Navbar} from "../common/Common";
import {Profile} from "./Profile";

ReactDOM.render(<Navbar showProfile={false}/>, document.getElementById('navbar'));

ReactDOM.render(<Profile/>, document.getElementById('app'));

serviceWorker.unregister();
