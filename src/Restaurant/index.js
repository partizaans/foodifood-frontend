import React from "react";
import ReactDOM from "react-dom";
import axios from 'axios'
import _ from 'lodash'
import PropTypes from 'prop-types'
import {PersianNumber} from 'react-persian-currency'

import {withRouter} from "react-router";


import {AuthenticatedComponent, Footer, Navbar} from "../common/Common";

class Restaurant extends AuthenticatedComponent {
  constructor(props) {
    super(props);
    this.restaurantID = this.props.match.params.id
    this.state = {restaurant: {ordinaryFoods: [], id: 0}}
  }

  render() {
    return (
      <>
        <div className="bg-pink">
          <p className="display-1"><br/></p>
          <p className="display-1"><br/></p>
        </div>
        <div className="text-center border-0 top-50">
          <img src={this.state.restaurant.logo} alt="khames" className="shadow-sm restaurant-logo"/>
          <h1 className="mt-3 font-weight-bolder">{this.state.restaurant.name}</h1>
        </div>
        <div className="row d-none d-xl-flex text-center m-0 mb-5">
          <div className="col-4"/>
          <div className="col-8">
            <h1 className="text-darkcyan">
                    <span className="border-bottom border-thick border-darkcyan px-4">
                        منوی غذا
                    </span>
            </h1>
          </div>
        </div>
        <div className="row text-center m-0 mb-5">
          <div className="col-xl-4">
            <div className="card ml-xlg-5 shadow rounded">
              <div className="card-body">
                <span className="h3 card-title border-bottom border-dark px-2">سبد خرید</span>
                <div className="px-5 mt-4 h4 font-weight-normal w-100 border border-dashed rounded-5 border-thick">
                  <table className="table table-responsive table-borderless">
                    <tr>
                      <td className="text-left w-100">پیتزا اعلا</td>
                      <td><i className="flaticon-plus text-darkcyan"/></td>
                      <td className="px-0">۳</td>
                      <td><i className="flaticon-minus text-pink"/></td>
                    </tr>
                    <tr className="border-bottom border-thick">
                      <td colSpan="4" className="w-100 py-2 small text-right">۲۴۰۰۰ تومان</td>
                    </tr>
                    <tr>
                      <td className="text-left w-100">پیتزا نیمه‌اعلا</td>
                      <td><i className="flaticon-plus text-darkcyan"/></td>
                      <td className="px-0">۳</td>
                      <td><i className="flaticon-minus text-pink"/></td>
                    </tr>
                    <tr className="border-bottom border-thick">
                      <td colSpan="4" className="w-100 py-2 small text-right">۲۰۰۰۰ تومان</td>
                    </tr>
                  </table>
                </div>
                <h4>
                  <span className="font-weight-normal">جمع کل: </span>
                  <span className="font-weight-bold">۴۴۰۰۰ تومان</span>
                </h4>
                <button className="btn bg-cyan text-light round py-2 px-3">
                  <span className="h3 font-weight-normal">تایید نهایی</span>
                </button>
              </div>
            </div>
          </div>
          <div className="col-xl-8 border-dashed border-right-0 border-top-0 border-bottom-0 border-secondary">
            <h1 className="text-darkcyan d-xl-none mt-5">
                    <span className="border-bottom border-thick border-darkcyan px-4">
                        منوی غذا
                    </span>
            </h1>
            <Menu items={this.state.restaurant.ordinaryFoods} restaurantID={this.state.restaurant.id}/>
          </div>
        </div>
      </>
    )
  }

  componentDidMount() {
    axios.get('http://localhost:8080/restaurant/' + this.restaurantID,
      {
        headers: {
          Authorization: localStorage.getItem('token')
        }
      })
      .then(response => this.setState({restaurant: response.data}))
  }

}

class Menu extends React.Component {

  render() {
    const toBeRendered = []
    const menuItems = []
    for (let i = 0; i < this.props.items.length; i++) {
      const menuItem = this.props.items[i];
      menuItems.push(<MenuItem name={menuItem.name} image={menuItem.image} price={menuItem.price} id={menuItem.id}
                               popularity={menuItem.popularity} restaurantID={this.props.restaurantID}/>)
    }

    let chunked = _.chunk(menuItems, 3)
    for (let i = 0; i < chunked.length; i++) {
      const theChunk = chunked[i];
      toBeRendered.push(<div className="row mt-5 pr-xlg-5 abbas">{theChunk}</div>)
    }

    return toBeRendered;
  }
}

Menu.propTypes = {
  items: PropTypes.array,
  restaurantID: PropTypes.string
}

class MenuItem extends React.Component {
  constructor(props) {
    super(props);
    this.addToCart = this.addToCart.bind(this)
  }

  render() {
    return (
      <div className="col-4">
        <div className="card shadow h-100">
          <div className="card-body">
            <img src={this.props.image} className="rounded" alt="pizza"/>
            <div className="my-2">
              <span className="font-weight-bold mr-4">{this.props.name}</span>
              <PersianNumber>{this.props.popularity}</PersianNumber> <i className="fa fa-star text-gold"/>
            </div>
            <div className="font-weight-light my-2">
              <PersianNumber>{this.props.price}</PersianNumber> تومان
            </div>
            <button className="btn bg-yellow rounded" onClick={this.addToCart}>افزودن به سبد خرید</button>
          </div>
        </div>
      </div>
    )

  }

  addToCart() {
    axios.post("http://localhost:8080/add-to-cart",
      {food_id: this.props.id, restaurant_id: this.props.restaurantID})
      .then((response) => {
          alert('Success!')
        }
      ).catch(function (error) {
      alert(error.response.data.message)
    })
  }
}

MenuItem.propTypes = {
  id: PropTypes.number,
  price: PropTypes.number,
  name: PropTypes.string,
  popularity: PropTypes.number,
  restaurantID: PropTypes.string,
  image: PropTypes.string,
}


ReactDOM.render(<Footer/>, document.getElementById('footer'))
ReactDOM.render(<Navbar/>, document.getElementById('navbar'))
export default withRouter(Restaurant)
