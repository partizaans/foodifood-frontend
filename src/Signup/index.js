import React from "react";
import ReactDOM from "react-dom";
import axios from 'axios'
import {withRouter} from "react-router";


import {Footer, Navbar} from "../common/Common";

class Signup extends React.Component {
  constructor(props) {
    super(props);
    this.handleFirstName = this.handleFirstName.bind(this)
    this.handleLastName = this.handleLastName.bind(this)
    this.handleEmail = this.handleEmail.bind(this)
    this.handlePhone = this.handlePhone.bind(this)
    this.handlePassword = this.handlePassword.bind(this)
    this.handleAddress = this.handleAddress.bind(this)
    this.onSubmit = this.onSubmit.bind(this)
    this.state = {firstName: "", lastName: "", email: "", phone: "", password: "", address: ""}
  }

  render() {
    return <div className="container h-100">
      <div className="jumbotron vh-100 text-center bg-white d-flex flex-column justify-content-center">
        <form onSubmit={this.onSubmit}>
          <div className="form align-items-center">
            <div className="form-row">
              <div className="form-group col-md-6">
                <label htmlFor="firstName">نام:</label>
                <input type="text" className="form-control" id="firstName"
                       placeholder="نام خود را وارد کنید!"
                       onChange={this.handleFirstName} name="firstName" required/>
              </div>
              <div className="form-group col-md-6">
                <label htmlFor="lastName">نام خانوادگی:</label>
                <input type="text" className="form-control" id="lastName"
                       placeholder="نام خانوادگی خود را وارد کنید!"
                       onChange={this.handleLastName} name="lastName" required/>
              </div>
            </div>
            <div className="form-row">
              <div className="form-group col-md-6">
                <label htmlFor="email">ایمیل:</label>
                <input type="email" className="form-control" id="email"
                       placeholder="ایمیل خود را وارد کنید!"
                       onChange={this.handleEmail} name="email" required/>
              </div>
              <div className="form-group col-md-6">
                <label htmlFor="phone">شماره تماس:</label>
                <input type="tel" className="form-control" id="phone"
                       placeholder="شماره تماس خود را وارد کنید!"
                       onChange={this.handlePhone} name="phone" required/>
              </div>
            </div>
            <div className="form-group">
              <label htmlFor="password">رمز عبور</label>
              <input type="password" className="form-control" id="password" placeholder="رمز عبور خود را وارد کنید!"
                     onChange={this.handlePassword} name="password" required minLength="4"/>
            </div>
            <div className="form-group">
              <label htmlFor="address">آدرس:</label>
              <input type="text" className="form-control" id="address" placeholder="آدرس خود را وارد کنید!"
                     onChange={this.handleAddress} name="address" required/>
            </div>
            <button type="submit" className="btn text-light bg-cyan">ثبت نام</button>
          </div>
        </form>
      </div>
    </div>
  }

  onSubmit(event) {
    event.preventDefault()
    if (!this.isValidPhone()) {
      alert('wrong mobile format')
      return
    }
    axios.post("http://localhost:8080/sign-up", this.state)
      .then((response) => {
        alert('Account created successfully')
        window.location.href = '/login'
      }).catch(function (error) {
      alert(error.response.data.message)
    })
  }

  handleFirstName(event) {
    let val = event.target.value
    this.setState(() => ({firstName: val}));
  }

  handleLastName(event) {
    let val = event.target.value
    this.setState(() => ({lastName: val}));
  }

  handleEmail(event) {
    let val = event.target.value
    this.setState(() => ({email: val}));
  }

  handlePhone(event) {
    let val = event.target.value
    this.setState(() => ({phone: val}));
  }

  handlePassword(event) {
    let val = event.target.value
    this.setState(() => ({password: val}));
  }

  handleAddress(event) {
    let val = event.target.value
    this.setState(() => ({address: val}));
  }

  isValidPhone() {
    let phone = this.state.phone
    return phone.length === 11 && ['۰', '0'].includes(phone[0]) && ['۹', '9'].includes(phone[1])
  }
}

ReactDOM.render(<Footer/>, document.getElementById('footer'))
ReactDOM.render(<Navbar/>, document.getElementById('navbar'))
export default withRouter(Signup)
