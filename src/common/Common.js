import React from 'react'
import PropTypes from 'prop-types'

import '../index';

export class Navbar extends React.Component {

  render() {
    const showProfile = this.props.showProfile;

    return (
      <nav className="navbar navbar-expand-sm navbar-light bg-white px-5 font-weight-normal h5 py-0 fixed-top">
        <a className="navbar-brand m-0" href="https://google.com">
          <img src={process.env.PUBLIC_URL + "/images/LOGO.png"} alt=""/>
        </a>
        <div className="navbar-nav ml-auto align-middle">
          <a className="nav-item nav-link active badged-item" href="#/">
            <i className="flaticon-smart-cart h3"/>
            <small className="inline-badge bg-cyan text-white small">۳</small>
          </a>
        </div>
        <div className="navbar-nav align-middle">
          {showProfile &&
          <a className="nav-item nav-link active" href="/profile">حساب کاربری</a>}
          <a className="nav-item nav-link text-pink" href='#' onClick={this.handleLogout}>خروج</a>
        </div>
      </nav>
    );
  }

  handleLogout() {
    localStorage.removeItem("token")
    window.location = '/login'
  }
}

Navbar.propTypes = {
  showProfile: PropTypes.bool
};

Navbar.defaultProps = {
  showProfile: true
};

export function isLoggedIn() {
  return localStorage.getItem("token")
}

export class Footer extends React.Component {
  render() {
    return (
      <footer className="footer bg-darkcyan">
        <div className="container text-light text-center">
          © تمامی حقوق متعلق به لقمه است.
        </div>
      </footer>
    )
  }
}


export class AuthenticatedComponent extends React.Component {
  constructor(props) {
    if (!isLoggedIn()) {
      window.location = '/login'
    }
    super(props);
  }
}

export class ContentSection extends React.Component {
  render() {
    return (
      <div className="mt-5">
        <div className="bg-pink">
          <p className="display-1"><br/></p>
          <p className="display-1"><br/></p>
        </div>
        <p className="display-1"><br/></p>
        <p className="display-1"><br/></p>
        <p className="display-1"><br/></p>
        <p className="display-1"><br/></p>
        <p className="display-1"><br/></p>
        <p className="display-1"><br/></p>
        <p className="display-1"><br/></p>
        <p className="display-1"><br/></p>
        <p className="display-1"><br/></p>
      </div>
    );
  }
}
