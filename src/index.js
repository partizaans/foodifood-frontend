import React from 'react';
import ReactDOM from 'react-dom';

import * as serviceWorker from './common/serviceWorker';
import {App} from "./App";

ReactDOM.render(
  <React.StrictMode>
    <App/>
  </React.StrictMode>,
  document.getElementById('app')
);

serviceWorker.unregister();
